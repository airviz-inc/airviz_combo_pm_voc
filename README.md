# Airviz.ComboPmVoc

Tools to work with Airviz Combo PM+VOC air quality monitor data.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `airviz_combo_pm_voc` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:airviz_combo_pm_voc, "~> 0.1.0"},
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/airviz_combo_pm_voc](https://hexdocs.pm/airviz_combo_pm_voc).

