defmodule Airviz.ComboPmVoc.MixProject do
  use Mix.Project

  def project do
    [
      app: :airviz_combo_pm_voc,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :inets]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # basic numerics
      {:exla, "~> 0.1", github: "elixir-nx/nx", sparse: "exla"},
      {:torchx, "~> 0.1", github: "elixir-nx/nx", sparse: "torchx"},
      {:nx, "~> 0.1", github: "elixir-nx/nx", sparse: "nx", override: true},
      # {:nx, "~> 0.1.0-dev", github: "dognotdog/nx", sparse: "nx", override: true, branch: "triangular-solve-dims-fix"},

      # visualization
      {:vega_lite, "~> 0.1"},
      {:kino, "~> 0.3"},
      # html/json parsing
      {:floki, "~> 0.31"},
      {:jason, "~> 1.2"},
      {:csv, "~> 3.0"},
      # httpoison for http_stream
      {:httpoison, "~> 1.7"},
      {:http_stream, "~> 1.0"},

      # parallelization
      {:parallel_stream, "~> 1.0"},

      # spectrometer tools
      {:spex, git: "git@github.com:CMU-CREATE-Lab/spex.git"},
    ]
  end
end
