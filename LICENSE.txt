This file is part of Airviz.ComboPmVoc. Copyright 2021 Airviz, Inc. 
Authors: 
    Dömötör Gulyas <dognotdog@gmail.com>

Airviz.ComboPmVoc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Airviz.ComboPmVoc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Airviz.ComboPmVoc.  If not, see <https://www.gnu.org/licenses/>.
