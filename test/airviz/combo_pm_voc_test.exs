defmodule Airviz.ComboPmVocTest do
  use ExUnit.Case
  doctest Airviz.ComboPmVoc

  test "greets the world" do
    assert Airviz.ComboPmVoc.hello() == :world
  end

  test "inner_join1" do
    result = PmVoc.TimeSeries.Stream.inner_join([[{0, 1, 2, 3}, {1, 1, 2, 3}], [{1, 4, 5, 6.2}, {2, 4.1, 5.2, 6.1}]])
    |> Enum.to_list()

    assert result == [{1, [{1, 2, 3}, {4, 5, 6.2}]}]
  end

  test "inner_join2" do
    result = PmVoc.TimeSeries.Stream.inner_join([[{0,1,2,3}, {1,1,2,3}]]) |> Enum.to_list()

    assert result == [{0, [{1, 2, 3}]}, {1, [{1, 2, 3}]}]
  end

end
