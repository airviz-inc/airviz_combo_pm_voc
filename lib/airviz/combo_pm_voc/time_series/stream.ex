defmodule Airviz.ComboPmVoc.TimeSeries.Stream do
  @moduledoc """
    Working with timeseries streams.
  """

  alias Airviz.ComboPmVoc.TimeSeries


  defstruct continuations: []

  defp get_time(tuple), do: elem(tuple, 0)

  @doc """
  Will do an inner join on timeseries streams where each row is of the form {timestamp | data},
  with the result being of the form {timestamp, [...]} for each of the input timeseries.
  """
  def inner_join(input_streams) do
    # we return a function that can be enumerated with Enumerable
    # for that we need the suspended continuations
    continuations = for input <- input_streams do
      {:suspended, [], continuation} = Enumerable.reduce(input, {:suspend, []}, fn (head, acc) -> {:suspend, [head | acc]} end)
      {[], continuation}
    end
    fn command, reducer ->
      state = %Airviz.ComboPmVoc.TimeSeries.Stream{
        continuations: continuations
      }
      merge_do(state, command, reducer)
    end
  end

  defp iterate_once(continuation) do
    case continuation.({:cont, []}) do
      {:suspended, val, cont} ->
        {val, cont}
      {:done, val} ->
        {val, nil}
      {:halted, val} ->
        {val, nil}
        end
  end

  defp merge_do(_state = %Airviz.ComboPmVoc.TimeSeries.Stream{continuations: continuations}, {:halt, acc}, _reducer) do
    # halt everything upstream, and we're done
    for {val, cont_fun} <- continuations do
      {:halted, _val} = cont_fun.({:halt, val})
    end
    
    {:halted, acc}
  end

  defp merge_do(state = %Airviz.ComboPmVoc.TimeSeries.Stream{}, {:suspend, acc}, reducer) do
    {:suspended, acc, &merge_do(state, &1, reducer)}
  end

  defp merge_do(state = %Airviz.ComboPmVoc.TimeSeries.Stream{continuations: continuations}, {:cont, acc}, reducer) do

    # iterate values until all have the same timestamp
    case iterate_until_same(continuations) do
      :done ->
        {:done, acc}
      continuations ->
        rows = [[timestamp | _] | _tail] = for {[val], _cont} <- continuations, do: Tuple.to_list(val)
        datas = for [_time | data] <- rows, do: List.to_tuple(data)
        merged_row = {timestamp, datas}

        # consume values from continuation list
        continuations = for {_val, cont} <- continuations, do: {[], cont}

        merge_do(%Airviz.ComboPmVoc.TimeSeries.Stream{state | continuations: continuations}, reducer.(merged_row, acc), reducer)        
    end
  end

  defp iterate_until_same(continuations) do
    # get new values if they were consumed before
    continuations = for continuation <- continuations do
      case continuation do
        {[], cont_fun} when cont_fun != nil -> # fetch next value when empty and have continuation
          iterate_once(cont_fun) 

        other -> # do nothing in other cases
          other
      end
    end

    # if any column missing a value, we're done
    case Enum.any?(continuations, fn {val, _cont} -> (val == []) || (val == nil) end) do
      true ->
        :done
      false ->
        times = Enum.map(continuations, fn {[val], _cont} -> 
          get_time(val) 
        end) 
        if Enum.count(times) > 0 do
          maxtime = times |> Enum.max()
          # IO.puts("maxtime = #{inspect(maxtime)}")
          iterate_until_same(continuations, maxtime)
        else
          :done
        end
    end
  end

  defp iterate_until_same(continuations, maxtime) do
    # get new values if they aren't maxtime
    continuations = for continuation <- continuations do
      case continuation do
        {[val], cont_fun} when cont_fun != nil -> #
          if get_time(val) < maxtime do
            iterate_once(cont_fun) 
          else
            {[val], cont_fun}
          end

        other -> # do nothing in other cases
          other
      end
    end

    # if any column missing a value, we're done
    case Enum.any?(continuations, fn {val, _cont} -> val == [] end) do
      true ->
        :done
      false ->
        # if all times are the same, we're good
        # otherwise try again iterating to the next timestamp
        case Enum.any?(continuations, fn {[val], _cont} -> get_time(val) != maxtime end) do
          false ->
            continuations
          true ->
            maxtime = Enum.map(continuations, fn {[val], _cont} -> get_time(val) end) |> Enum.max()
            iterate_until_same(continuations, maxtime)
        end
    end
  end

  @doc """
  Chunk a timeseries by where `gap` is exceeded between samples.
  """
  def chunk_with_gap(series, gap)

  def chunk_with_gap(sx = %Nx.Tensor{}, gap), do: chunk_with_gap(TimeSeries.from_tensor(sx), gap)

  def chunk_with_gap(series, gap) do
    series
    |> Stream.chunk_while(
      %{last_time: 0, parts: []}, 
      fn
        row, %{parts: []} ->
          t = get_time(row)
          # new chunk
          {:cont, %{last_time: t, parts: [row]}}
        row, %{last_time: tl, parts: parts} when elem(row,0)-tl > gap ->
          t = get_time(row)
          # if we're not 30s apart in timestamps, there was a gap
          {:cont, Enum.reverse(parts), %{last_time: t, parts: [row]}}
        row, %{parts: parts} ->
          t = get_time(row)
          # add chunk
          {:cont, %{last_time: t, parts: [row | parts]}}
      end,
      fn 
        %{parts: []} -> {:cont, %{}}
        %{parts: parts} -> {:cont, Enum.reverse(parts), %{}}
      end
    )
  end

  @doc """
  flatten rows to the format {time, tuple} from {time, list_of_tuples}.
  """
  def flatten_rows(series) do
    series
    |> Stream.map(fn {time, datas} -> 
      row = for data <- datas do
        Tuple.to_list(data)
      end
      |> List.to_tuple()
      {time, row}
    end)
  end

end
