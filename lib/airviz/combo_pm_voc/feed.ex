defmodule Airviz.ComboPmVoc.Feed do

  require Jason

  alias Airviz.ComboPmVoc.Cache

	 def fetch_feed_descriptor(feed_key) do
    url = "https://esdr.cmucreatelab.org/api/v1/feeds/#{feed_key}"
    {:ok, {{_, 200, _}, _headers, json_data}} = :httpc.request(url)
    {:ok, info} = Jason.decode(json_data)

    info
  end

  def feed_name(_feed_descriptor = %{data: %{name: name}}) do
    name
  end
  def feed_channels(_feed_descriptor = %{"data" => %{"channelBounds" => %{"channels" => channel_bounds}}}) do
    Map.keys(channel_bounds)
  end

  def fetch_json!(url) do
    {:ok, {{_, 200, _}, _headers, json_data}} = :httpc.request(url)
    content = Jason.decode!(json_data)
    # IO.puts("fetched #{inspect(content)}")
    content
  end

  def stream_csv!(url) do
    HTTPStream.get(url)
    |> CSV.decode!(headers: false)
    # drop headers
    |> Stream.drop(1)
    |> Stream.map(fn row ->
      # IO.puts("#{inspect(row)}")
      # for {_name, x} <- row do
      for x <- row do
        case x do
          # some values might be missing.
          # while treating them as 0 might not be the best,
          # we don't have a better way for now
          "" -> 0.0
          _ -> 
            {fx, _string} = Float.parse(x)
            fx
        end
      end
    end)
  end

  def fetch_feed_data(api_key, channels) do
    :ok = File.mkdir_p(Cache.base_path())
    file_path = Path.join([Cache.base_path(), "#{api_key}.ebt"])

    if !File.exists?(file_path) do
      channel_str = Enum.join(channels, ",")

      # fetch CSV, because we can stream it, unlike json
      data = stream_csv!("https://esdr.cmucreatelab.org/api/v1/feeds/#{api_key}/channels/#{channel_str}/export?format=csv")

      # create streamable chunks of data by encoding each row separately
      # with a 32-bit header for packet size
      data
      |> Stream.map(fn row ->
        pack_row(row)
      end)
      |> Stream.into(File.stream!(file_path))
      |> Stream.run()
    end

    file_path
  end

  @doc """
  convert erlang term into binary packet with size header for saving into files
  """
  def pack_row(row) do
    bin = :erlang.term_to_binary(row)
    <<byte_size(bin)::32-little, bin::binary>>
  end

  defp take_packets(
    packets,
    <<packet_size::32-little, term_data::size(packet_size)-binary, rest::binary>>
  ) do
    packet = :erlang.binary_to_term(term_data)
    take_packets([packet | packets], rest)
  end

  defp take_packets(packets, other) do
    {Enum.reverse(packets), other}
  end

  @spec load_feed_data(String.t()) :: Enumerable.t()
  def load_feed_data(api_key) do
    file_path = Path.join([Cache.base_path(), "#{api_key}.ebt"])

    load_terms_array(file_path)
  end

  @spec load_terms_array(String.t()) :: Enumerable.t()
  def load_terms_array(file_path) do
    IO.puts("loading feed data from file #{inspect(file_path)}")

    # read packets from stream and transform to terms
    File.stream!(file_path, [], 1_048_576)
    |> Stream.transform(%{data: <<>>}, fn
      new_data, %{data: old_data} ->
        data = old_data <> new_data
        {packets, remaining_data} = take_packets([], data)
        {packets, %{data: remaining_data}}
    end)
  end

  def fetch_feeds!(sensors) do
    sensors
    |> ParallelStream.map(fn {name, src_feed_info = %{all_feeds: all_feeds}} ->
    # |> Stream.map(fn {_name, feed_info = %{}} ->
      # find out what kind of feed we've got
      pm_names_list = ["voltage"]
      pm_names_set = MapSet.new(pm_names_list)
      voc_names_list = ["pressure", "temp", "humidity", "tvoc"]
      voc_names_set = MapSet.new(voc_names_list)
      pmvoc_names_list = ["sharp_voltage", "pressure_pa_ch3", "temperature_k_ch3", "humidity_percent_ch3","tvoc_ch0"]
      pmvoc_names_set = MapSet.new(pmvoc_names_list)
      sorted_feeds = for feed_key <- all_feeds do
        feed_descriptor = fetch_feed_descriptor(feed_key)
        channels = feed_channels(feed_descriptor)
        if MapSet.equal?(MapSet.intersection(MapSet.new(channels), pmvoc_names_set), pmvoc_names_set) do
          {:pmvoc_feeds, feed_key}
        else
          if MapSet.equal?(MapSet.intersection(MapSet.new(channels), voc_names_set), voc_names_set) do
            {:voc_feeds, feed_key}

          else
            if MapSet.equal?(MapSet.intersection(MapSet.new(channels), pm_names_set), pm_names_set) do
              {:pm_feeds, feed_key}
            else
              nil
            end
          end
        end
      end
      # remap sorted feeds list into map
      feed_info = for {key, feed_key} <- sorted_feeds, reduce: %{} do
        acc -> Map.update(acc, key, [feed_key], &([feed_key |  &1]))
      end

      case feed_info do
        %{pm_feeds: pm_feeds} ->
          for api_key <- pm_feeds do
            fetch_feed_data(api_key, pm_names_list)
          end
        _ -> true
      end

      case feed_info do
        %{voc_feeds: voc_feeds} ->
          for api_key <- voc_feeds do
            fetch_feed_data(api_key, voc_names_list)
          end
        _ -> true
      end

      case feed_info do
        %{pmvoc_feeds: pmvoc_feeds} ->
          for api_key <- pmvoc_feeds do
            fetch_feed_data(api_key, pmvoc_names_list)
          end
        _ -> true
      end
      {name, Map.merge(src_feed_info, feed_info)}
    end)
    |> Enum.to_list()
  end


  def voltage_cache_file(name) do
    "#{Cache.base_path()}/#{name}.voltage.30s.ebt"
  end

  def pthv_cache_file(name) do
    "#{Cache.base_path()}/#{name}.pressure.temperature.humidity.tvoc.30s.ebt"
  end

  def pmvoc_cache_file(name) do
    "#{Cache.base_path()}/#{name}.voltage.pressure.temperature.humidity.tvoc.30s.ebt"
  end

  defp aggregate_pm({name, %{pm_feeds: pm_feeds}}) do
    for api_key <- pm_feeds do
      IO.puts("name = #{inspect(name)}")
      file_path = voltage_cache_file(name)
      IO.puts("  api_key = #{inspect(api_key)}")

      if !File.exists?(file_path) do
        out_stream = File.stream!(file_path, [:delayed_write])

        api_key
        |> load_feed_data()
        |> Stream.chunk_by(fn
          [x, _y] ->
            div(trunc(x), 30)

          _ ->
            nil
        end)
        |> ParallelStream.map(fn chunk ->
          [[t0, _] | _] = chunk
          time = div(trunc(t0), 30) * 30
          {_t, y} = Enum.unzip(Enum.map(chunk, fn [x, y] -> {x, y} end))
          n = Enum.count(y)
          yy = for val <- y, do: val * val
          term = {time, Enum.sum(y) / n, Enum.sum(yy) / n, n}
          pack_row(term)
        end)
        |> Stream.into(out_stream)
        |> Stream.run()
      end
    end
  end
  defp aggregate_pm({_name, %{}}), do: nil

  defp aggregate_voc({name, %{voc_feeds: voc_feeds}}) do
    for api_key <- voc_feeds do
      file_path = pthv_cache_file(name)

      if !File.exists?(file_path) do
        IO.puts("PTHV api_key = #{inspect(api_key)}")
        data = load_feed_data(api_key)
        # IO.puts("#{inspect(feed)}")
        # IO.puts("data = #{inspect(data)}")

        data
        |> Stream.chunk_by(fn [x | _y] -> div(trunc(x), 30) end)
        |> Stream.map(fn chunk ->
          [[t0 | _] | _] = chunk
          time = div(trunc(t0), 30) * 30
          # IO.puts("chunk = #{inspect(chunk)}")

          # {_t, p, t, h, v} = List.unzip(Enum.map(chunk, fn [x, p, t, h, v] -> {x, p, t, h, v} end))
          # in the feed, data is as follows: pressure, temperature, humidity, voc
          [_t, p, t, h, voc] =
            Enum.reduce(chunk, [[], [], [], [], []], fn [x, p, t, h, v], [xx, pp, tt, hh, vv] ->
              [[x | xx], [p | pp], [t | tt], [h | hh], [v | vv]]
            end)

          n = Enum.count(p)
          pp = for val <- p, do: val * val
          tt = for val <- t, do: val * val
          hh = for val <- h, do: val * val
          vv = for val <- voc, do: val * val

          term = {
            time,
            Enum.sum(p) / n,
            Enum.sum(t) / n,
            Enum.sum(h) / n,
            Enum.sum(voc) / n,
            Enum.sum(pp) / n,
            Enum.sum(tt) / n,
            Enum.sum(hh) / n,
            Enum.sum(vv) / n,
            n
          }

          pack_row(term)
        end)
        |> Stream.into(File.stream!(file_path, [:delayed_write]))
        |> Stream.run()
      end
    end
  end
  defp aggregate_voc({_name, %{}}), do: nil

  defp aggregate_pmvoc({name, %{pmvoc_feeds: pmvoc_feeds}}) do
    for api_key <- pmvoc_feeds do
      file_path = pmvoc_cache_file(name)

      if !File.exists?(file_path) do
        IO.puts("PMVOC api_key = #{inspect(api_key)}")
        data = load_feed_data(api_key)
        # IO.puts("#{inspect(feed)}")
        # IO.puts("data = #{inspect(data)}")

        data
        |> Stream.chunk_by(fn [x | _y] -> div(trunc(x), 30) end)
        |> Stream.map(fn chunk ->
          [[t0 | _] | _] = chunk
          time = div(trunc(t0), 30) * 30
          # IO.puts("chunk = #{inspect(chunk)}")

          # fields (plus x/time at beginning):
          # ["sharp_voltage", "pressure_pa_ch3", "temperature_k_ch3", "humidity_percent_ch3", "tvoc_ch0"]
          [_t, v, p, t, h, voc] =
            Enum.reduce(chunk, [[], [], [], [], [], []], fn [x, v, p, t, h, voc], [xx, vv, pp, tt, hh, vvoc] ->
              # make sure voc is a float
              vocf = 1.0*voc
              # voltage is expected to be in volts, not millivolts
              # downstream from here
              v = 1.0*v
              [[x | xx], [v | vv], [p | pp], [t | tt], [h | hh], [vocf | vvoc]]
            end)

          n = 1.0*Enum.count(p)
          # IO.puts("p = #{inspect(p)}")
          # IO.puts("voc = #{inspect(voc)}")
          pp = for val <- p, do: val * val
          tt = for val <- t, do: val * val
          hh = for val <- h, do: val * val
          vv = for val <- v, do: val * val
          vvoc = for val <- voc, do: val * val
          # nv = max(1, Enum.count(for val when val != nil <- voc, do: 1))
          term = {
            time,
            Enum.sum(v) / n,
            Enum.sum(p) / n,
            Enum.sum(t) / n,
            Enum.sum(h) / n,
            Enum.sum(voc) / n,
            Enum.sum(vv) / n,
            Enum.sum(pp) / n,
            Enum.sum(tt) / n,
            Enum.sum(hh) / n,
            Enum.sum(vvoc) / n,
            n
          }

          pack_row(term)
        end)
        |> Stream.into(File.stream!(file_path, [:delayed_write]))
        |> Stream.run()
      end
    end
  end
  defp aggregate_pmvoc({_name, %{}}), do: nil

  @doc """
  This function creates 30s aggregate chunks from the 1s sample time data,
  to reduce data volume for further processing.

  Files are cached.
  """
  def aggregate(sensors) do
    # generate all voltage CSVs
    sensors
    |> ParallelStream.map(fn item ->
        aggregate_pm(item)
        aggregate_voc(item)
        aggregate_pmvoc(item)
    end)
    |> Stream.run()
  end
end
