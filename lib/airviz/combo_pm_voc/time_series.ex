defmodule Airviz.ComboPmVoc.TimeSeries do
  @moduledoc """
    Working with timeseries.
  """

  require Nx

  @doc """
  convert tensor to array of {time, data = {...}} tuples
  """
  def from_tensor(tensor = %Nx.Tensor{}) do
    {nrows, _ncols} = Nx.shape(tensor)
    rows = for row <- 0..nrows-1 do
      [time | data] = tensor[row] |> Nx.to_flat_list()
      {time, List.to_tuple(data)}
    end
    rows
  end


  def daily_chunks(timeseries) do
    timeseries
    |> Stream.chunk_by(fn {t, _data} ->
      DateTime.from_unix!(t) |> DateTime.to_date()
    end)
  end

  defp maybe_start_date(_days = []) do
    nil
  end

  defp maybe_start_date([[{t, _data} | _] | _]) do
    DateTime.from_unix!(t) |> DateTime.to_date()
  end

  defp maybe_emit_week(acc_days, new_day = [{nt, _} | _]) do
    new_date = DateTime.from_unix!(nt) |> DateTime.to_date()
    n = Enum.count(acc_days)
    days = Enum.reverse(acc_days)

    remaining_days =
      Enum.filter(days, fn [{t, _} | _] ->
        date = DateTime.from_unix!(t) |> DateTime.to_date()
        Date.diff(new_date, date) < 7
      end)

    new_days = [new_day | remaining_days |> Enum.reverse()]
    acc = {maybe_start_date(remaining_days), new_days}

    cond do
      n == 7 ->
        {:cont, days |> Enum.concat(), acc}

      true ->
        # if we don't have enough days, don't emit
        {:cont, acc}
    end
  end

  def daily_weekly_chunks(timeseries) do
    timeseries
    |> daily_chunks()
    |> Stream.chunk_while(
      {nil, []},
      fn
        day = [{t, _} | _], {nil, []} ->
          # no previous days, just start collecting
          date = DateTime.from_unix!(t) |> DateTime.to_date()
          {:cont, {date, [day]}}

        day, {_beginning, days} ->
          # have some days buffered
          maybe_emit_week(days, day)
      end,
      fn
        {nil, []} ->
          {:cont, {nil, []}}

        {_date, days} ->
          case Enum.count(days) do
            7 ->
              {:cont, days |> Enum.reverse() |> Enum.concat(), {nil, []}}

            _ ->
              {:cont, {nil, []}}
          end
      end
    )
  end

  def mean_time(nil) do
    nil
  end

  def mean_time([]) do
    nil
  end

  def mean_time(chunk) do
    s =
      for {t, _data} <- chunk, reduce: 0 do
        acc -> acc + t
      end

    s / Enum.count(chunk)
  end
end
