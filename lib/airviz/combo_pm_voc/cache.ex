defmodule Airviz.ComboPmVoc.Cache do
  @cache_path "airviz_combo_pm_voc_cache"

  @doc """
  base cache path
  """
  def base_path() do
    @cache_path
  end

end