defmodule Airviz.ComboPmVoc.Baseline do
  @moduledoc """
  Baselining PM data.
  """

  require Spex
  alias VegaLite, as: Vl

  defp voltage_regression(voltage, temperature, humidity) do
    # {minx, maxx} = Enum.min_max(t)
    # IO.puts("data starts at #{DateTime.from_unix!(minx, :second)}")
    # IO.puts("data ends   at #{DateTime.from_unix!(maxx, :second)}")

    ones = Nx.tensor([for(_i <- 1..Enum.count(temperature), do: 1.0e-0)])
    tx = Nx.tensor(temperature) |> Nx.reshape(Nx.shape(ones))
    hx = Nx.tensor(humidity) |> Nx.reshape(Nx.shape(ones))
    y = Nx.tensor(voltage) |> Nx.reshape(Nx.shape(ones)) |> Nx.transpose()
    x = Nx.concatenate([ones, tx, hx], axis: 0) |> Nx.transpose()
    b = Spex.Regression.linreg(x, y)
    # yy = Nx.dot(x, b)
    {x, b}
  end

  defp voltage_regression(chunk) do
    # t = for {t, _data} <- chunk, do: t
    {voltage, temperature, humidity} = vth(chunk)
    voltage_regression(voltage, temperature, humidity)
  end

  @doc """
  this function calculates a regression using less than percentile data points, 
  rejecting those with a larger value incrementally
  """
  def voltage_baseline_regression(chunk, percentile) do
    # t = for {t, _data} <- chunk, do: t
    {voltage, temperature, humidity} = vth(chunk)

    # it might be the case that we've got empties
    case Enum.count(temperature) do
      0 -> {0.0,0}
      _ ->
        x0 = voltage_regressor(temperature, humidity)

        b =
          voltage_baseline_regression_do(
            {voltage, temperature, humidity},
            {x0, voltage, temperature, humidity},
            percentile
          )

        {x0, b}
    end
  end

  defp voltage_regressor(temperature, humidity) do
    ones = Nx.tensor([for(_i <- 1..Enum.count(temperature), do: 1.0e-0)])
    tx = Nx.tensor(temperature) |> Nx.reshape(Nx.shape(ones))
    hx = Nx.tensor(humidity) |> Nx.reshape(Nx.shape(ones))
    # y = Nx.tensor(voltage) |> Nx.reshape(Nx.shape(ones)) |> Nx.transpose()
    x = Nx.concatenate([ones, tx, hx], axis: 0) |> Nx.transpose()
    x
  end

  defp voltage_regressor(chunk) do
    {_voltage, temperature, humidity} = vth(chunk)

    voltage_regressor(temperature, humidity)
  end

  defp vth(chunk) do
    voltage = for {_t, data} <- chunk do
      case data do
        [voltage_data, _env_data] -> elem(voltage_data, 0)
        [combo_data] -> elem(combo_data, 0)
      end
   end
    temperature = for {_t, data} <- chunk do
      case data do
        [_voltage_data, env_data] -> elem(env_data, 1)
        [combo_data] -> elem(combo_data, 2)
      end
    end
    humidity = for {_t, data} <- chunk do
      case data do
        [_voltage_data, env_data] -> elem(env_data, 2)
        [combo_data] -> elem(combo_data, 3)
      end
    end
    {voltage, temperature, humidity}
  end

  defp voltage_baseline_regression_do(
         {voltage, temperature, humidity},
         {x0, voltage0, temperature0, humidity0},
         percentile
       ) do
    {_x, b} = voltage_regression(voltage, temperature, humidity)

    yy = Nx.dot(x0, b)

    selector =
      Enum.zip(voltage0, yy |> Nx.to_flat_list())
      |> Enum.map(fn {src, model} -> src <= model end)

    fraction_ok = Enum.count(selector, & &1) / Enum.count(selector)

    if fraction_ok <= percentile do
      b
    else
      voltage =
        Enum.zip(voltage0, selector)
        |> Enum.filter(fn {_x, selected?} -> selected? end)
        |> Enum.map(fn {x, _selected?} -> x end)

      temperature =
        Enum.zip(temperature0, selector)
        |> Enum.filter(fn {_x, selected?} -> selected? end)
        |> Enum.map(fn {x, _selected?} -> x end)

      humidity =
        Enum.zip(humidity0, selector)
        |> Enum.filter(fn {_x, selected?} -> selected? end)
        |> Enum.map(fn {x, _selected?} -> x end)

      voltage_baseline_regression_do(
        {voltage, temperature, humidity},
        {x0, voltage0, temperature0, humidity0},
        percentile
      )
    end
  end

  def regress_pm(chunk) do
    {_x, b} = voltage_baseline_regression(chunk, 0.1)
    b
  end

  def plot_regressed_pm(chunk, b) do
    t = for {t, _data} <- chunk, do: t * 1000
    {minx, maxx} = Enum.min_max(t)
    # IO.puts("data starts at #{DateTime.from_unix!(minx, :millisecond)}")
    # IO.puts("data ends   at #{DateTime.from_unix!(maxx, :millisecond)}")
    {voltage, temperature, humidity} = vth(chunk)

    x = voltage_regressor(temperature, humidity)
    yy = Nx.dot(x, b)

    Vl.new(width: 700, height: 400)
    # |> Vl.resolve(scale: [y: :independent])
    |> Vl.data_from_series(
      x: t,
      voltage: voltage,
      temperature: temperature,
      voltage_reg: Nx.to_flat_list(yy)
    )
    |> Vl.layers([
      Vl.new()
      |> Vl.mark(:line, color: :red)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x",
        type: :temporal,
        scale: [domain: [minx, maxx]],
        time_unit: "yearmonthdatehoursminutesseconds"
      )
      |> Vl.encode_field(:y, "voltage", type: :quantitative, scale: [zero: false]),
      Vl.new()
      |> Vl.mark(:line, color: :green)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      |> Vl.encode_field(:y, "voltage_reg", type: :quantitative, scale: [zero: false])
      # Vl.new(resolve: [scale: [y: :independent]])
      # |> Vl.mark(:line)
      # |> Vl.encode(:size, value: 1)
      # |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      # |> Vl.encode_field(:y, "temperature", type: :quantitative, scale: [zero: false]),
    ])
  end

  def plot_regressed_pm(chunk) do
    t = for {t, _data} <- chunk, do: trunc(t) * 1000
    {minx, maxx} = Enum.min_max(t)
    # IO.puts("data starts at #{DateTime.from_unix!(minx, :millisecond)}")
    # IO.puts("data ends   at #{DateTime.from_unix!(maxx, :millisecond)}")

    pm = for {_t, {_raw_, _baseline, _signal, pm, _voc, _temp, _humidity, _pressure}} <- chunk, do: pm


    Vl.new(width: 700, height: 400)
    # |> Vl.resolve(scale: [y: :independent])
    |> Vl.data_from_series(
      x: t,
      pm: pm
    )
    |> Vl.layers([
      Vl.new()
      |> Vl.mark(:line, color: :red, clip: true)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x",
        type: :temporal,
        scale: [domain: [minx, maxx]],
        time_unit: "yearmonthdatehoursminutesseconds"
      )
      |> Vl.encode_field(:y, "pm", type: :quantitative, scale: [zero: false, domain: [-10.0, 100.0]]),
      # Vl.new()
      # |> Vl.mark(:line, color: :green)
      # |> Vl.encode(:size, value: 1)
      # |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      # |> Vl.encode_field(:y, "voltage_reg", type: :quantitative, scale: [zero: false]),
      # Vl.new(resolve: [scale: [y: :independent]])
      # |> Vl.mark(:line)
      # |> Vl.encode(:size, value: 1)
      # |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      # |> Vl.encode_field(:y, "temperature", type: :quantitative, scale: [zero: false]),
    ])
  end


  def plot_regression_params(regressions) do
    t = for {t, _data} <- regressions, do: t * 1000
    {minx, maxx} = Enum.min_max(t)
    # IO.puts("data starts at #{DateTime.from_unix!(minx, :millisecond)}")
    # IO.puts("data ends   at #{DateTime.from_unix!(maxx, :millisecond)}")
    cr = for {_t, b} <- regressions, do: b[0] |> Nx.to_flat_list() |> Enum.at(0)
    tr = for {_t, b} <- regressions, do: b[1] |> Nx.to_flat_list() |> Enum.at(0)
    hr = for {_t, b} <- regressions, do: b[2] |> Nx.to_flat_list() |> Enum.at(0)

    Vl.new(width: 700, height: 400)
    |> Vl.resolve(:scale, y: :independent)
    |> Vl.data_from_series(
      x: t,
      cr: cr,
      tr: tr,
      hr: hr
    )
    |> Vl.layers([
      Vl.new()
      |> Vl.mark(:line, color: :red)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x",
        type: :temporal,
        scale: [domain: [minx, maxx]],
        time_unit: "yearmonthdatehoursminutesseconds"
      )
      |> Vl.encode_field(:y, "tr", type: :quantitative, scale: [zero: false]),
      Vl.new()
      |> Vl.mark(:line, color: :green)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      |> Vl.encode_field(:y, "cr", type: :quantitative, scale: [zero: false])
      # Vl.new(resolve: [scale: [y: :independent]])
      # |> Vl.mark(:line)
      # |> Vl.encode(:size, value: 1)
      # |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      # |> Vl.encode_field(:y, "temperature", type: :quantitative, scale: [zero: false]),
    ])
  end

  def plot_baselined_pm_data(baselined_data) do
    t = for item <- baselined_data, do: elem(item, 0) * 1000
    {minx, maxx} = Enum.min_max(t)
    # IO.puts("data starts at #{DateTime.from_unix!(minx, :millisecond)}")
    # IO.puts("data ends   at #{DateTime.from_unix!(maxx, :millisecond)}")
    baseline = for {_t, data} <- baselined_data, do: elem(data, 1)
    signal = for {_t, data} <- baselined_data, do: elem(data, 2)
    pm = for {_t, data} <- baselined_data, do: elem(data, 3)

    Vl.new(width: 700, height: 400)
    |> Vl.resolve(:scale, y: :independent)
    |> Vl.data_from_series(
      x: t,
      baseline: baseline,
      signal: signal,
      pm: pm
    )
    |> Vl.layers([
      Vl.new()
      |> Vl.mark(:line, color: :red)
      |> Vl.encode(:size, value: 1)
      |> Vl.encode_field(:x, "x",
        type: :temporal,
        scale: [domain: [minx, maxx]],
        time_unit: "yearmonthdatehoursminutesseconds"
      )
      |> Vl.encode_field(:y, "baseline", type: :quantitative, scale: [zero: false]),
      Vl.new()
      |> Vl.mark(:circle, color: :green)
      |> Vl.encode(:size, value: 2)
      |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      |> Vl.encode_field(:y, "pm", type: :quantitative, scale: [zero: false])
      # Vl.new(resolve: [scale: [y: :independent]])
      # |> Vl.mark(:line)
      # |> Vl.encode(:size, value: 1)
      # |> Vl.encode_field(:x, "x", type: :temporal, scale: [domain: [minx, maxx]])
      # |> Vl.encode_field(:y, "temperature", type: :quantitative, scale: [zero: false]),
    ])
  end

  def convert_regressions_to_nx(regressions) do
    for {t, b} <- regressions do
      [t | Nx.to_flat_list(b)]
    end
    |> Nx.tensor()
  end

  def baseline_pm_data(timeseries, rx) do
    alias Spex.Interp

    # rx = convert_regressions_to_nx(regressions)
    rt = rx[[0..-1//1, 0..0]] |> Nx.reshape({elem(Nx.shape(rx), 0)})
    rb = rx[[0..-1//1, 1..-1//1]]

    IO.puts("timeseries = #{inspect(Enum.to_list(timeseries))}")
    for {time, data} <- timeseries do
      {v,t,h,p,voc} = case data do
        [vdat, edat] ->
          # elements: voltage
          v = elem(vdat, 0)
          # elements: pressure, temperature, humidity, voc
          p = elem(edat, 0)*100.0
          t = elem(edat, 1)+273.15
          h = elem(edat, 2)
          voc = elem(edat, 3)
          {v,t,h,p,voc}
        [dat] ->
          # elements: voltage, pressure, temperature, humidity, voc
          v = elem(dat, 0)
          p = elem(dat, 1)
          t = elem(dat, 2)
          h = elem(dat, 3)
          voc = elem(dat, 4)
          {v,t,h,p,voc}
      end

      # interpolate baseline voltages, not regression parameters
      vb =
        Interp.interp_f(time, rt, rb, fn y ->
          # map to voltage
          [a, b, c] = Nx.to_flat_list(y)
          a + b * t + c * h
        end)
        |> Nx.to_number()

      # sensitivity is 5mV/ug
      # (from 0.5V / (100ug / m^3) in datasheet)
      signal = v - vb
      pm = signal / 0.005
      # IO.puts("out = #{inspect({time, {vb, signal, pm}})}")

      {time, {v, vb, signal, pm, t, h, p, voc}}
    end
  end
end