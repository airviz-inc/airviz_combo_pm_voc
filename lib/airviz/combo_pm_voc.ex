defmodule Airviz.ComboPmVoc do
  @moduledoc """
  Documentation for `Airviz.ComboPmVoc`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Airviz.ComboPmVoc.hello()
      :world

  """
  def hello do
    :world
  end
end
